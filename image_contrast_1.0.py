"""
该程序用于将给定图片与预设图片进行姿态对比
给定图片存放位置：E:\Backups\Python\Deep-Learning\MoveNet_Pytorch\images
预设图片存放位置：E:\Backups\Python\Deep-Learning\MoveNet_Pytorch\images_data

原理：
考虑到姿态的细小差异，要在预设图片文件夹中多存放一些相似姿态的预设图片，这样可以增加对比准确度
给定的图片会和预设图片文件夹内图片一一比较，成功则输出True

相比与 image_contrast_base.py 的变化：
1.移除不需要的库
2.移除生成图像的功能，仅仅进行姿态对比
"""
import time # 导入time库，用于计算程序运行时间
import argparse # 导入argparse库，用于解析命令行参数
import os # 导入os库，用于文件操作
import torch # 导入torch库，用于深度学习模型
import numpy as np # 导入numpy库，用于数值计算
from movenet.models.model_factory import load_model # 导入自定义模型加载函数
from movenet.utils import read_imgfile, draw_skel_and_kp # 导入自定义图像读取和绘制函数

# 解析命令行参数
parser = argparse.ArgumentParser() # 创建ArgumentParser对象
parser.add_argument('--model', type=str, default="movenet_lightning", choices=["movenet_lightning", "movenet_thunder"]) # 添加模型参数
parser.add_argument('--conf_thres', type=float, default=0.3) # 添加置信度阈值参数
parser.add_argument('--image_dir', type=str, default='./images') # 添加输入图像文件夹路径参数
parser.add_argument('--output_dir', type=str, default='./output') # 添加输出图像文件夹路径参数
args = parser.parse_args() # 解析参数并保存到args对象

# 根据选择的模型设置输入图像尺寸和特征图尺寸
if args.model == "movenet_lightning":
    args.size = 192
    args.ft_size = 48
else:
    args.size = 256
    args.ft_size = 64

def compute_similarity(pose1, pose2): # 姿态比较
    # 计算欧几里得距离矩阵
    distance_matrix = np.linalg.norm(pose1[:, :2] - pose2[:, :2], axis=1)
    # 平均距离作为相似度
    similarity = np.mean(distance_matrix)
    # 将距离映射到0-1之间的相似度值，距离越小，相似度越高
    similarity = 1.0 / (1.0 + similarity)
    return similarity

def main(): # 主函数
    model = load_model(args.model, ft_size=args.ft_size) # 加载指定模型
    if args.output_dir:
        if not os.path.exists(args.output_dir):
            os.makedirs(args.output_dir)

    reference_images_dir = 'E:\Backups\Python\Deep-Learning\MoveNet_Pytorch\images_data' # 预设图片文件夹路径
    reference_images = [
        f.path for f in os.scandir(reference_images_dir) if f.is_file() and f.path.endswith(('.png', '.jpg', 'jpeg'))] # 获取预设图片文件列表

    reference_poses = [] # 保存预设图片的姿态信息
    for ref_image in reference_images:
        ref_input_image, _ = read_imgfile(ref_image, args.size) # 读取预设图片并进行预处理
        with torch.no_grad():
            ref_input_image = torch.Tensor(ref_input_image) # 将图像转换为张量
            ref_kpt_with_conf = model(ref_input_image)[0, 0, :, :].numpy() # 使用模型预测关键点位置
            reference_poses.append(ref_kpt_with_conf) # 将预测结果添加到列表中

    filenames = [
        f.path for f in os.scandir(args.image_dir) if f.is_file() and f.path.endswith(('.png', '.jpg', 'jpeg'))] # 获取输入图像文件列表
    start = time.time() # 记录程序开始时间
    for f in filenames:
        input_image, draw_image = read_imgfile(f, args.size) # 读取输入图像并进行预处理
        with torch.no_grad():
            input_image = torch.Tensor(input_image) # 将图像转换为张量
            kpt_with_conf = model(input_image)[0, 0, :, :] # 使用模型预测关键点位置
            kpt_with_conf = kpt_with_conf.numpy() # 将结果转换为NumPy数组
        print("Image:", f,end=' ')

        similarity_threshold = 0.88 # 相似度阈值
        for ref_pose in reference_poses:
            similarity = compute_similarity(ref_pose, kpt_with_conf) # 计算输入图像与预设图片的姿态相似度
            if similarity >= similarity_threshold:
                print("是否相似：True")  # 如果达到相似度阈值，则输出True
                break  # 停止对比其他预设图片
        else:
            print("是否相似：False")  # 如果没有达到相似度阈值，则输出False
        print('——————————————————————————')

if __name__ == "__main__":
    main()
